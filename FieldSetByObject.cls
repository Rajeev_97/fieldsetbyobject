public class FieldSetByObject{
    public static void getFieldSetByObject(){
        Map<String, Map<String, Schema.FieldSet>>fieldSetByObjectAndLabel = new Map<String, Map<String, Schema.FieldSet>>();
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        for(String typeName : globalDescribe.keySet()) {
            Schema.SObjectType sObjectType = globalDescribe.get(typeName);
            Schema.DescribeSObjectResult sObjectResult = sObjectType.getDescribe();
            if (sObjectResult.isCustom() && sObjectResult.getName().endsWith('__x')) {
                fieldSetByObjectAndLabel.put(sObjectResult.getName(), sObjectResult.fieldSets.getMap());
            }
        }
    } 
}